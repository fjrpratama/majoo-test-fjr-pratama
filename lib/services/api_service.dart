import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/models/news_response_mdl.dart';
import 'package:majootestcase/services/dio_config_service.dart' as dioConfig;

class ApiServices {
  Future<MovieResponse> getMovieList() async {
    try {
      var dio = await dioConfig.dio();
      Response<String> response = await dio.get("title/auto-complete?q=game-of-th");
      MovieResponse movieResponse =
          MovieResponse.fromJson(jsonDecode(response.data!));
      return movieResponse;
    } catch (e) {
      print(e.toString());
      return throw Exception('failed');
    }
  }
  Future<NewsResponseMdl> getNews() async {
    try {
      var dio = await dioConfig.dio();
      Response<String> response = await dio.get("title/get-news?tconst=tt0944947&limit=25");
      NewsResponseMdl newsResponse =
          NewsResponseMdl.fromJson(jsonDecode(response.data!));
      return newsResponse;
    } catch (e) {
      print(e.toString());
      return throw Exception('failed');
    }
  }
}
