class Preference {
  static const USER_INFO = "user-info";
}

class Api {
  static const BASE_URL = "https://imdb8.p.rapidapi.com/";
  static const LOGIN = "/login";
  static const REGISTER = "/register";
  static const rapidAPIKey = "a9cde18f23msh8419f60e1855a21p1ae576jsnc586529b5023";
  static const rapidAPIHost = "imdb8.p.rapidapi.com";
}

class Font {
}

class ScreenUtilConstants {
  static const width = 320.0;
  static const height = 640.0;
}
