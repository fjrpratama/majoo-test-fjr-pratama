class NewsResponseMdl {
  String? type;
  List<Items>? items;
  String? label;
  String? paginationKey;
  String? id;
  Image? image;
  String? title;
  String? titleType;
  int? year;

  NewsResponseMdl(
      {this.type,
      this.items,
      this.label,
      this.paginationKey,
      this.id,
      this.image,
      this.title,
      this.titleType,
      this.year});

  NewsResponseMdl.fromJson(Map<String, dynamic> json) {
    type = json['@type'];
    if (json['items'] != null) {
      items = <Items>[];
      json['items'].forEach((v) {
        items!.add(new Items.fromJson(v));
      });
    }
    label = json['label'];
    paginationKey = json['paginationKey'];
    id = json['id'];
    image = json['image'] != null ? new Image.fromJson(json['image']) : null;
    title = json['title'];
    titleType = json['titleType'];
    year = json['year'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['@type'] = this.type;
    if (this.items != null) {
      data['items'] = this.items!.map((v) => v.toJson()).toList();
    }
    data['label'] = this.label;
    data['paginationKey'] = this.paginationKey;
    data['id'] = this.id;
    if (this.image != null) {
      data['image'] = this.image!.toJson();
    }
    data['title'] = this.title;
    data['titleType'] = this.titleType;
    data['year'] = this.year;
    return data;
  }
}

class Items {
  String? body;
  String? head;
  String? id;
  Image? image;
  String? link;
  String? publishDateTime;
  Source? source;

  Items(
      {this.body,
      this.head,
      this.id,
      this.image,
      this.link,
      this.publishDateTime,
      this.source});

  Items.fromJson(Map<String, dynamic> json) {
    body = json['body'];
    head = json['head'];
    id = json['id'];
    image = json['image'] != null ? new Image.fromJson(json['image']) : null;
    link = json['link'];
    publishDateTime = json['publishDateTime'];
    source =
        json['source'] != null ? new Source.fromJson(json['source']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['body'] = this.body;
    data['head'] = this.head;
    data['id'] = this.id;
    if (this.image != null) {
      data['image'] = this.image!.toJson();
    }
    data['link'] = this.link;
    data['publishDateTime'] = this.publishDateTime;
    if (this.source != null) {
      data['source'] = this.source!.toJson();
    }
    return data;
  }
}

class Image {
  int? height;
  String? id;
  String? url;
  int? width;

  Image({this.height, this.id, this.url, this.width});

  Image.fromJson(Map<String, dynamic> json) {
    height = json['height'];
    id = json['id'];
    url = json['url'];
    width = json['width'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['height'] = this.height;
    data['id'] = this.id;
    data['url'] = this.url;
    data['width'] = this.width;
    return data;
  }
}

class Source {
  String? id;
  String? label;
  String? link;

  Source({this.id, this.label, this.link});

  Source.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    label = json['label'];
    link = json['link'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['label'] = this.label;
    data['link'] = this.link;
    return data;
  }
}