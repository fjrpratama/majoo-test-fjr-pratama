import 'package:flutter/material.dart';

class CustomTextfield extends StatelessWidget {
  final TextEditingController? controller;
  final Widget? prefixIcon;
  final Widget? suffixIcon;
  final bool? obsecureText;
  final String? errorText;
  final String? hintText;
  final TextInputType? keyboardType;
  const CustomTextfield(
      {Key? key,
      this.controller,
      this.prefixIcon,
      this.suffixIcon,
      this.obsecureText,
      this.errorText,
      this.hintText,
      this.keyboardType})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          decoration: BoxDecoration(
              color: Colors.green.shade50,
              borderRadius: BorderRadius.circular(8)),
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: Row(
            children: [
              prefixIcon ?? SizedBox.shrink(),
              SizedBox(
                width: 10,
              ),
              Expanded(
                child: TextField(
                  keyboardType: keyboardType,
                  controller: controller,
                  obscureText: obsecureText ?? false,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: hintText,
                  ),
                ),
              ),
              SizedBox(
                width: 10,
              ),
              suffixIcon ?? SizedBox.shrink()
            ],
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Text(
          errorText ?? '',
          style: TextStyle(color: Colors.red, fontSize: 12),
        )
      ],
    );
  }
}
