import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/ui/extra/internet_down_screen.dart';
import 'package:majootestcase/ui/movie/movie_detail_page.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:majootestcase/utils/local_asset.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool _hasConnection = true;

  @override
  void initState() {
    super.initState();
    _checkConnection();
  }

  _getData() {
    BlocProvider.of<HomeBlocCubit>(context)..fetchingData();
  }

  _checkConnection() async {
    bool _result = await InternetConnectionChecker().hasConnection;
    if (!_result) {
      _hasConnection = false;
      setState(() {});
    } else {
      _getData();
      _hasConnection = true;
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: Row(
            children: [
              SizedBox(
                width: 30,
                height: 30,
                child: Image.asset(
                  LocalAsset.imgMajoo,
                  fit: BoxFit.contain,
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                "Movie App - Majoo Indonesia",
                style: TextStyle(color: Colors.black, fontSize: 16),
              )
            ],
          ),
          elevation: 1,
        ),
        body: _hasConnection
            ? RefreshIndicator(
                onRefresh: () async {
                  _checkConnection();
                },
                child: Scrollbar(
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        BlocBuilder<HomeBlocCubit, HomeBlocState>(
                          builder: (context, state) {
                            if (state is HomeBlocInitialState) {
                              return SizedBox(
                                width: double.infinity,
                                height: 150,
                                child: Center(
                                  child: CircularProgressIndicator(
                                    valueColor:
                                        AlwaysStoppedAnimation(Colors.blue),
                                  ),
                                ),
                              );
                            } else if (state is HomeBlocErrorState) {
                              return SizedBox(
                                width: double.infinity,
                                height: ScreenUtilConstants.height,
                                child: Column(
                                  children: [
                                    SizedBox(
                                      height: 200,
                                      width: 200,
                                      child: Image.asset(
                                        LocalAsset.imgError,
                                        fit: BoxFit.contain,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text("Cant get any data, please try again"),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 20, vertical: 20),
                                      child: CustomButton(
                                        height: 50,
                                        leadingIcon: CircularProgressIndicator
                                            .adaptive(),
                                        onPressed: _getData,
                                        text: 'Coba Lagi',
                                      ),
                                    )
                                  ],
                                ),
                              );
                            } else if (state is HomeBlocLoadedState) {
                              return GridView.count(
                                shrinkWrap: true,
                                physics: NeverScrollableScrollPhysics(),
                                crossAxisCount: 2,
                                childAspectRatio: 6 / 8,
                                crossAxisSpacing: 10,
                                mainAxisSpacing: 10,
                                children: state.data.data!
                                    .map(
                                      (e) => InkWell(
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (_) =>
                                                      MovieDetailPage(
                                                        movieData: e,
                                                      )));
                                        },
                                        child: Container(
                                          margin: EdgeInsets.symmetric(
                                              horizontal: 10, vertical: 10),
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              boxShadow: [
                                                BoxShadow(
                                                    blurRadius: 5,
                                                    color: Colors.black
                                                        .withOpacity(.1))
                                              ],
                                              borderRadius:
                                                  BorderRadius.circular(8)),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              SizedBox(
                                                width:
                                                    ScreenUtilConstants.width,
                                                height: 150,
                                                child: ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(8),
                                                  child: Hero(
                                                    tag: e.id!,
                                                    child: FadeInImage
                                                        .assetNetwork(
                                                      placeholder: LocalAsset
                                                          .imgPlaceholder,
                                                      image: e.i!.imageUrl ??
                                                          'https://m.media-amazon.com/images/M/MV5BZDAwNDhiNWMtMTQyNi00M2YyLWE5MWEtYTdiNjk2OWVkNDBhXkEyXkFqcGdeQXVyMTE0MzQwMjgz._V1_.jpg',
                                                      fit: BoxFit.cover,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: Text(
                                                  e.l!,
                                                  style: TextStyle(
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.w700),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    )
                                    .toList(),
                              );
                            }
                            return Container();
                          },
                        )
                      ],
                    ),
                  ),
                ),
              )
            : InternetDownScreenPage(
                onTap: _checkConnection,
              ));
  }
}
