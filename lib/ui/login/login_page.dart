import 'dart:convert';

import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/custom_textfield.dart';
import 'package:majootestcase/database/db_helper.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/ui/home/home_page.dart';
import 'package:majootestcase/ui/main/main_page.dart';
import 'package:majootestcase/ui/register/register_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  String? _emailErrorText;
  String? _passwordErrorText;

  bool? _isObscurePassword = true;

  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
  }

  _saveLoginData() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.setBool("is_logged_in", true);
  }

  _saveUserData(User user) async {
    SharedPreferences _pref = await SharedPreferences.getInstance();
    String data = jsonEncode(user.toJson());

    await _pref.setString('userData', data);
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            backgroundColor: Colors.white,
            elevation: 0,
          ),
          body: BlocListener<AuthBlocCubit, AuthBlocState>(
            listener: (context, state) {
              if (state is LoginLoadingState) {
                setState(() {
                  _isLoading = true;
                });
              }

              if (state is LoginFailedState) {
                setState(() {
                  _isLoading = false;
                });

                ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(content: Text('Gagal Melakukan Login')));
              }

              if (state is LoginSuccessState) {
                setState(() {
                  _isLoading = false;
                });

                _saveLoginData();
                _saveUserData(state.user!);

                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(
                        builder: (_) => BlocProvider(
                              create: (_) => HomeBlocCubit(),
                              child: MainPage(),
                            )),
                    (route) => false);

                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: Text('Berhasil Login'),
                  backgroundColor: Colors.green,
                ));
              }
            },
            child: SingleChildScrollView(
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Selamat Datang',
                      style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                        // color: colorBlue,
                      ),
                    ),
                    Text(
                      'Silahkan login terlebih dahulu',
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    SizedBox(
                      height: 35,
                    ),
                    CustomTextfield(
                      controller: _emailController,
                      errorText: _emailErrorText,
                      keyboardType: TextInputType.emailAddress,
                      hintText: 'masukkan email anda',
                      prefixIcon: Icon(
                        Icons.email,
                        size: 20,
                        color: Colors.teal.shade700,
                      ),
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    CustomTextfield(
                      controller: _passwordController,
                      errorText: _passwordErrorText,
                      hintText: 'masukkan password anda',
                      obsecureText: _isObscurePassword,
                      keyboardType: TextInputType.visiblePassword,
                      suffixIcon: IconButton(
                        icon: Icon(
                          Icons.visibility,
                          size: 20,
                          color: Colors.teal.shade700,
                        ),
                        onPressed: () {
                          setState(() {
                            _isObscurePassword = !_isObscurePassword!;
                          });
                        },
                      ),
                      prefixIcon: Icon(
                        Icons.vpn_key,
                        size: 20,
                        color: Colors.teal.shade700,
                      ),
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    CustomButton(
                      height: 50,
                      // leadingIcon: Icon(Icons.login, color: Colors.white, size: 20,),
                      onPressed: () {
                        _loginHandle();
                      },
                      text: 'Login',
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Center(
                      child: TextButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (_) => BlocProvider(
                                        create: (_) => AuthBlocCubit(),
                                        child: RegisterPage(),
                                      )));
                        },
                        child: Text('Belum punya akun? daftar dulu disini'),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
        _isLoading
            ? Container(
                width: double.infinity,
                height: double.infinity,
                decoration: BoxDecoration(color: Colors.black.withOpacity(.7)),
                child: Center(
                  child: CircularProgressIndicator.adaptive(
                    strokeWidth: 5,
                    valueColor: AlwaysStoppedAnimation(Colors.greenAccent),
                  ),
                ),
              )
            : SizedBox.shrink()
      ],
    );
  }

  _loginHandle() {
    print('clicked');
    if (_emailController.text == '' || _passwordController.text == '') {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('Form Tidak Boleh Kosong')));
    } else {
      if (_checkEmail(_emailController.text) == true) {
        setState(() {
          _emailErrorText = null;
        });
        BlocProvider.of<AuthBlocCubit>(context).loginUser(
            email: _emailController.text, password: _passwordController.text);
      } else {
        _emailErrorText = 'Masukkan e-mail yang valid';
        setState(() {});
      }
    }
  }

  bool? _checkEmail(String value) {
    bool _valid = EmailValidator.validate(value);
    return _valid;
  }
}
