import 'package:flutter/material.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:majootestcase/utils/local_asset.dart';

class MovieDetailPage extends StatefulWidget {
  final Data? movieData;
  const MovieDetailPage({Key? key, this.movieData}) : super(key: key);

  @override
  _MovieDetailPageState createState() => _MovieDetailPageState();
}

class _MovieDetailPageState extends State<MovieDetailPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          body: CustomScrollView(
        physics: BouncingScrollPhysics(),
        slivers: [
          SliverAppBar(
            iconTheme: IconThemeData(color: Colors.white),
            backgroundColor: Colors.white,
            title: Text(
              "Movie Detail",
              style: TextStyle(fontSize: 16),
            ),
            elevation: 1,
            expandedHeight: ScreenUtilConstants.width,
            stretch: true,
            flexibleSpace: FlexibleSpaceBar(
              stretchModes: [StretchMode.zoomBackground],
              background: Container(
                width: double.infinity,
                height: ScreenUtilConstants.width,
                child: Hero(
                  tag: widget.movieData!.id ?? 'movie tag',
                  child: FadeInImage.assetNetwork(
                    placeholder: LocalAsset.imgPlaceholder,
                    image: widget.movieData!.i!.imageUrl ??
                        'https://m.media-amazon.com/images/M/MV5BZDAwNDhiNWMtMTQyNi00M2YyLWE5MWEtYTdiNjk2OWVkNDBhXkEyXkFqcGdeQXVyMTE0MzQwMjgz._V1_.jpg',
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.movieData!.l ?? "Movie Title",
                    style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Wrap(
                    spacing: 5,
                    children: [
                      Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 20, vertical: 2.5),
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                                colors: [Colors.greenAccent, Colors.teal]),
                            borderRadius: BorderRadius.circular(25)),
                        child:
                            Text("Year : " + widget.movieData!.year.toString()),
                      ),
                      Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 20, vertical: 2.5),
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                                colors: [Colors.amber, Colors.green]),
                            borderRadius: BorderRadius.circular(25)),
                        child:
                            Text("Rank : " + widget.movieData!.rank.toString()),
                      )
                    ],
                  ),
                  Divider(),
                  Text(
                    widget.movieData!.s ?? 'Movie Description',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                  Divider(),
                  widget.movieData!.series != null
                      ? Text(
                          'Series',
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        )
                      : SizedBox.shrink()
                ],
              ),
            ),
          ),
          widget.movieData!.series != null
              ? SliverList(
                  delegate: SliverChildBuilderDelegate((context, index) {
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Row(
                      children: [
                        Container(
                          width: 100,
                          height: 125,
                          margin: EdgeInsets.symmetric(vertical: 5),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(8),
                            child: FadeInImage.assetNetwork(
                              placeholder: LocalAsset.imgPlaceholder,
                              image:
                                  widget.movieData!.series![index].i!.imageUrl!,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                            child: Text(
                          widget.movieData!.series![index].l!,
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ))
                      ],
                    ),
                  );
                }, childCount: widget.movieData!.series!.length))
              : SliverToBoxAdapter(
                  child: SizedBox.shrink(),
                )
        ],
      )),
    );
  }
}
