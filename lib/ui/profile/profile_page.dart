import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/ui/login/login_page.dart';
import 'package:majootestcase/utils/local_asset.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  User _userData = new User();
  bool? _isLoading = true;

  @override
  void initState() {
    super.initState();
    _getData();
  }

  _getData() async {
    setState(() {
      _isLoading = true;
    });
    SharedPreferences _pref = await SharedPreferences.getInstance();
    String? data = _pref.getString('userData');

    var _temp = User.fromJson(jsonDecode(data!));

    setState(() {
      _userData = _temp;
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Row(
          children: [
            SizedBox(
              width: 30,
              height: 30,
              child: Image.asset(
                LocalAsset.imgMajoo,
                fit: BoxFit.contain,
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Text(
              "Profile",
              style: TextStyle(color: Colors.black, fontSize: 16),
            )
          ],
        ),
        elevation: 1,
      ),
      body: BlocListener<AuthBlocCubit, AuthBlocState>(
        listener: (context, state) {
          if (state is LogoutSuccessState) {
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                    builder: (_) => BlocProvider(
                          create: (_) => AuthBlocCubit(),
                          child: LoginPage(),
                        )),
                (route) => false);
          }
        },
        child: Column(
          children: [
            SizedBox(
              width: double.infinity,
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      width: 70,
                      height: 70,
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                              colors: [Colors.greenAccent, Colors.teal],
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter),
                          shape: BoxShape.circle),
                      child: Center(
                        child: Icon(
                          Icons.account_circle_outlined,
                          color: Colors.white,
                          size: 40,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Expanded(
                        child: Column(
                      children: [
                        Text(
                          _isLoading == true
                              ? "...Mengambil data"
                              : _userData.name!,
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                        Text(
                          _isLoading == true
                              ? "...Mengambil data"
                              : _userData.email!,
                          style: TextStyle(fontSize: 14),
                        ),
                      ],
                      crossAxisAlignment: CrossAxisAlignment.start,
                    ))
                  ],
                ),
              ),
            ),
            Container(
              width: double.infinity,
              height: 10,
              decoration: BoxDecoration(color: Colors.teal.withOpacity(.4)),
            ),
            Padding(
              padding: EdgeInsets.all(20),
              child: CustomButton(
                height: 45,
                isSecondary: true,
                leadingIcon: Icon(Icons.exit_to_app),
                onPressed: _logout,
                text: 'Logout',
              ),
            ),
            Spacer(),
            Center(
              child: Text('edited to Null Safety by Fajar Pratama'),
            ),
            SizedBox(
              height: 20,
            )
          ],
        ),
      ),
    );
  }

  _logout() {
    BlocProvider.of<AuthBlocCubit>(context)..logoutUser();
  }
}
