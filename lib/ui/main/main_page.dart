import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/ui/home/home_page.dart';
import 'package:majootestcase/ui/news/news_page.dart';
import 'package:majootestcase/ui/profile/profile_page.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int _index = 0;
  PageController _pageController = PageController(initialPage: 0);

  @override
  void initState() {
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        controller: _pageController,
        physics: NeverScrollableScrollPhysics(),
        children: [
          HomePage(),
          NewsPage(),
          BlocProvider(
            create: (_) => AuthBlocCubit(),
            child: ProfilePage(),
          )
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _index,
        onTap: (index) {
          _pageController.jumpToPage(index);
          setState(() {
            _index = index;
          });
        },
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.movie), label: 'Movie', tooltip: 'Movie Page'),
          BottomNavigationBarItem(
              icon: Icon(Icons.feed), label: 'News', tooltip: 'News Page'),
          BottomNavigationBarItem(
              icon: Icon(Icons.account_box_outlined),
              label: 'Profile',
              tooltip: 'Profile Page'),
        ],
      ),
    );
  }
}
