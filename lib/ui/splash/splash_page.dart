import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/ui/home/home_page.dart';
import 'package:majootestcase/ui/login/login_page.dart';
import 'package:majootestcase/ui/main/main_page.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:majootestcase/utils/local_asset.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    super.initState();
    _checkLogin();
  }

  _checkLogin() {
    BlocProvider.of<AuthBlocCubit>(context)..fetchLoginHistory();
  }

  _changePage(Widget page) {
    Timer(Duration(seconds: 5), () {
      Navigator.pushAndRemoveUntil(
          context, MaterialPageRoute(builder: (_) => page), (route) => false);
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            body: BlocListener<AuthBlocCubit, AuthBlocState>(
                listener: (context, state) {
                  if (state is AuthBlocLoginState) {
                    _changePage(BlocProvider(
                      create: (_) => AuthBlocCubit(),
                      child: LoginPage(),
                    ));
                  }
                  if (state is AuthBlocLoggedInState) {
                    _changePage(BlocProvider(
                      create: (_) => HomeBlocCubit(),
                      child: MainPage(),
                    ));
                  }
                },
                child: Container(
                  width: double.infinity,
                  height: ScreenUtilConstants.height,
                  color: Colors.white,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        width: 125,
                        height: 125,
                        child: Center(
                          child: Image.asset(
                            LocalAsset.imgMajoo,
                            fit: BoxFit.contain,
                          ),
                        ),
                      )
                    ],
                  ),
                ))));
  }
}
