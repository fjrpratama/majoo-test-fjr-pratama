import 'package:flutter/material.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/utils/local_asset.dart';

class InternetDownScreenPage extends StatefulWidget {
  final VoidCallback? onTap;
  const InternetDownScreenPage({Key? key, this.onTap}) : super(key: key);

  @override
  _InternetDownScreenPageState createState() => _InternetDownScreenPageState();
}

class _InternetDownScreenPageState extends State<InternetDownScreenPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        width: double.infinity,
        child: Column(
          children: [
            SizedBox(
              height: 250,
              width: 250,
              child: Image.asset(
                LocalAsset.imgInternet,
                fit: BoxFit.contain,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              'Koneksi Anda Mengalami Gangguan',
              style: TextStyle(fontSize: 20),
            ),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: CustomButton(
                height: 50,
                onPressed: widget.onTap,
                text: 'Try Again',
              ),
            )
          ],
        ),
      ),
    );
  }
}
