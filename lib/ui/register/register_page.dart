import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/custom_textfield.dart';
import 'package:majootestcase/database/db_helper.dart';
import 'package:majootestcase/models/user.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  TextEditingController _nameController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  String? _emailErrorText;
  String? _nameErrorText;
  String? _passwordErrorText;

  bool? _isObscurePassword = true;

  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            backgroundColor: Colors.white,
            elevation: 0,
          ),
          body: BlocListener<AuthBlocCubit, AuthBlocState>(
            listener: (context, state) {
              if (state is RegisterLoadingState) {
                setState(() {
                  _isLoading = true;
                });
              }

              if (state is RegisterFailedState) {
                setState(() {
                  _isLoading = false;
                });

                ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(content: Text('Gagal Melakukan Register')));
              }

              if (state is RegisterSuccessState) {
                setState(() {
                  _isLoading = false;
                });
                Navigator.pop(context);
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: Text('Berhasil Membuat Akun Baru, silahkan login'),
                  backgroundColor: Colors.green,
                ));
              }
            },
            child: SingleChildScrollView(
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Daftar Akun Anda',
                      style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                        // color: colorBlue,
                      ),
                    ),
                    SizedBox(
                      height: 35,
                    ),
                    CustomTextfield(
                      controller: _nameController,
                      errorText: _nameErrorText,
                      keyboardType: TextInputType.text,
                      hintText: 'masukkan nama anda',
                      prefixIcon: Icon(
                        Icons.account_circle_outlined,
                        size: 20,
                        color: Colors.teal.shade700,
                      ),
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    CustomTextfield(
                      controller: _emailController,
                      errorText: _emailErrorText,
                      keyboardType: TextInputType.emailAddress,
                      hintText: 'masukkan email anda',
                      prefixIcon: Icon(
                        Icons.email,
                        size: 20,
                        color: Colors.teal.shade700,
                      ),
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    CustomTextfield(
                      controller: _passwordController,
                      errorText: _passwordErrorText,
                      hintText: 'masukkan password anda',
                      obsecureText: _isObscurePassword,
                      keyboardType: TextInputType.visiblePassword,
                      suffixIcon: IconButton(
                        icon: Icon(
                          Icons.visibility,
                          size: 20,
                          color: Colors.teal.shade700,
                        ),
                        onPressed: () {
                          setState(() {
                            _isObscurePassword = !_isObscurePassword!;
                          });
                        },
                      ),
                      prefixIcon: Icon(
                        Icons.vpn_key,
                        size: 20,
                        color: Colors.teal.shade700,
                      ),
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    CustomButton(
                      height: 50,
                      // leadingIcon: Icon(Icons.login, color: Colors.white, size: 20,),
                      onPressed: () {
                        _regsiterHandle();
                      },
                      text: 'Register',
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Center(
                      child: TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: Text('Sudah punya akun? Login disini'),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
        _isLoading
            ? Container(
                width: double.infinity,
                height: double.infinity,
                decoration: BoxDecoration(color: Colors.black.withOpacity(.7)),
                child: Center(
                  child: CircularProgressIndicator.adaptive(
                    strokeWidth: 5,
                    valueColor: AlwaysStoppedAnimation(Colors.greenAccent),
                  ),
                ),
              )
            : SizedBox.shrink()
      ],
    );
  }

  _regsiterHandle() {
    print('clicked');
    if (_emailController.text == '' ||
        _passwordController.text == '' ||
        _nameController.text == '') {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('Form Tidak Boleh Kosong')));
    } else {
      if (_checkEmail(_emailController.text) == true) {
        setState(() {
          _emailErrorText = null;
        });
        BlocProvider.of<AuthBlocCubit>(context).daftarAkun(
            user: User(
                email: _emailController.text,
                password: _passwordController.text,
                name: _nameController.text));
      } else {
        _emailErrorText = 'Masukkan e-mail yang valid';
        setState(() {});
      }
    }
  }

  bool? _checkEmail(String value) {
    bool _valid = EmailValidator.validate(value);
    return _valid;
  }
}
