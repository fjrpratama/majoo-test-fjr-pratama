import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/ui/extra/internet_down_screen.dart';
import 'package:majootestcase/ui/news/news_detail_page.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:majootestcase/utils/local_asset.dart';

class NewsPage extends StatefulWidget {
  const NewsPage({Key? key}) : super(key: key);

  @override
  _NewsPageState createState() => _NewsPageState();
}

class _NewsPageState extends State<NewsPage> {
  bool _hasConnection = true;

  @override
  void initState() {
    super.initState();
    _checkConnection();
  }

  _getData() {
    BlocProvider.of<HomeBlocCubit>(context)..fetchingNewsData();
  }

  _checkConnection() async {
    bool _result = await InternetConnectionChecker().hasConnection;
    if (!_result) {
      _hasConnection = false;
      setState(() {});
    } else {
      _getData();
      _hasConnection = true;
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: Row(
            children: [
              SizedBox(
                width: 30,
                height: 30,
                child: Image.asset(
                  LocalAsset.imgMajoo,
                  fit: BoxFit.contain,
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                "News",
                style: TextStyle(color: Colors.black, fontSize: 16),
              )
            ],
          ),
          elevation: 1,
        ),
        body: _hasConnection
            ? BlocBuilder<HomeBlocCubit, HomeBlocState>(
                builder: (context, state) {
                  if (state is GetNewsLoadingState) {
                    return SizedBox(
                      width: double.infinity,
                      height: 150,
                      child: Center(
                        child: CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation(Colors.blue),
                        ),
                      ),
                    );
                  } else if (state is GetNewsErrorState) {
                    return SizedBox(
                      width: double.infinity,
                      height: ScreenUtilConstants.height,
                      child: Column(
                        children: [
                          SizedBox(
                            height: 200,
                            width: 200,
                            child: Image.asset(
                              LocalAsset.imgError,
                              fit: BoxFit.contain,
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text("Cant get any data, please try again"),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 20, vertical: 20),
                            child: CustomButton(
                              height: 50,
                              leadingIcon: CircularProgressIndicator.adaptive(),
                              onPressed: _getData,
                              text: 'Coba Lagi',
                            ),
                          )
                        ],
                      ),
                    );
                  } else if (state is GetNewsLoadedState) {
                    return RefreshIndicator(
                      onRefresh: () async {
                        _checkConnection();
                      },
                      child: Scrollbar(
                        child: ListView.builder(
                          itemBuilder: (context, index) {
                            if (index == 0) {
                              return InkWell(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (_) => NewsDetailPage(
                                                items: state.data.items![index],
                                              )));
                                },
                                child: Container(
                                  margin: EdgeInsets.only(top: 20),
                                  width: double.infinity,
                                  height: 250,
                                  child: Column(
                                    children: [
                                      SizedBox(
                                        width: ScreenUtilConstants.width,
                                        height: 175,
                                        child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(8),
                                          child: Hero(
                                            tag: state
                                                .data.items![index].image!.url!,
                                            child: FadeInImage.assetNetwork(
                                                fit: BoxFit.cover,
                                                placeholder:
                                                    LocalAsset.imgPlaceholder,
                                                image: state.data.items![index]
                                                    .image!.url!),
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 20),
                                        child: Text(
                                          state.data.items![index].head!,
                                          maxLines: 2,
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                      Divider()
                                    ],
                                  ),
                                ),
                              );
                            } else {
                              return InkWell(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (_) => NewsDetailPage(
                                                items: state.data.items![index],
                                              )));
                                },
                                child: Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 20, vertical: 10),
                                  width: double.infinity,
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      SizedBox(
                                        width: 70,
                                        height: 100,
                                        child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(8),
                                          child: Hero(
                                            tag: state
                                                .data.items![index].image!.url!,
                                            child: FadeInImage.assetNetwork(
                                                fit: BoxFit.cover,
                                                placeholder:
                                                    LocalAsset.imgPlaceholder,
                                                image: state.data.items![index]
                                                    .image!.url!),
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Expanded(
                                          child: Text(
                                        state.data.items![index].head!,
                                        maxLines: 2,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      ))
                                    ],
                                  ),
                                ),
                              );
                            }
                          },
                          itemCount: state.data.items!.length,
                        ),
                      ),
                    );
                  }

                  return Container();
                },
              )
            : InternetDownScreenPage(
                onTap: _checkConnection,
              ));
  }
}
