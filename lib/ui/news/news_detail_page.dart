import 'package:flutter/material.dart';
import 'package:majootestcase/models/news_response_mdl.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:majootestcase/utils/local_asset.dart';

class NewsDetailPage extends StatelessWidget {
  final Items? items;
  const NewsDetailPage({Key? key, this.items}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: CustomScrollView(
          physics: BouncingScrollPhysics(),
          slivers: [
            SliverAppBar(
              iconTheme: IconThemeData(color: Colors.white),
              backgroundColor: Colors.white,
              title: Text(
                "News Detail",
                style: TextStyle(fontSize: 16),
              ),
              elevation: 1,
              expandedHeight: ScreenUtilConstants.width,
              stretch: true,
              flexibleSpace: FlexibleSpaceBar(
                stretchModes: [StretchMode.zoomBackground],
                background: Container(
                  width: double.infinity,
                  height: ScreenUtilConstants.width,
                  child: Hero(
                    tag: items!.image!.url ?? 'news tag',
                    child: FadeInImage.assetNetwork(
                      placeholder: LocalAsset.imgPlaceholder,
                      image: items!.image!.url!,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
            ),
            SliverToBoxAdapter(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                    child: Text(
                      items!.head ?? "Movie Title",
                      style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Divider(),
                  Padding(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                    child: Text(
                      items!.body ?? 'Movie Description',
                      style: TextStyle(fontSize: 14),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
