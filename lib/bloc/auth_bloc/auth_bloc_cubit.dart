import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/database/db_helper.dart';
import 'package:majootestcase/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  void fetchLoginHistory() async {
    emit(AuthBlocInitialState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool? isLoggedIn = sharedPreferences.getBool("is_logged_in");
    if (isLoggedIn == null) {
      emit(AuthBlocLoginState());
    } else {
      if (isLoggedIn) {
        emit(AuthBlocLoggedInState());
      } else {
        emit(AuthBlocLoginState());
      }
    }
  }

  void loginUser({String? email, String? password}) async {
    try {
      emit(LoginLoadingState());
      var _dbHelper = DbHelper();

      await _dbHelper
          .getLoginUser(email ?? '', password ?? '')
          .then((value) => emit(LoginSuccessState(user: value)));
      // print(data);
    } catch (e) {
      print("Error : " + e.toString());
      emit(LoginFailedState());
    }
  }

  void daftarAkun({User? user}) async {
    try {
      var _dbHelper = DbHelper();
      emit(RegisterLoadingState());

      await _dbHelper.saveData(user!).then((value) {
        emit(RegisterSuccessState());
      });
    } catch (e) {
      print("Error : " + e.toString());
      emit(LoginFailedState());
    }
  }

  void logoutUser() async {
    emit(LogoutLoadingState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.setBool("is_logged_in", false);
    emit(LogoutSuccessState());
  }
}
