part of 'auth_bloc_cubit.dart';

abstract class AuthBlocState extends Equatable {
  const AuthBlocState();

  @override
  List<Object> get props => [];
}

class AuthBlocInitialState extends AuthBlocState {}

class AuthBlocLoadingState extends AuthBlocState {}

class AuthBlocLoggedInState extends AuthBlocState {}

class AuthBlocLoginState extends AuthBlocState {}

class AuthBlocSuccesState extends AuthBlocState {}

class AuthBlocLoadedState extends AuthBlocState {
  final data;

  AuthBlocLoadedState(this.data);
}

class AuthBlocErrorState extends AuthBlocState {
  final error;

  AuthBlocErrorState(this.error);
}

class LoginLoadingState extends AuthBlocState {}

class LoginFailedState extends AuthBlocState {}

class LoginSuccessState extends AuthBlocState {
  final User? user;
  LoginSuccessState({this.user});
}

class RegisterLoadingState extends AuthBlocState {}

class RegisterFailedState extends AuthBlocState {}

class RegisterSuccessState extends AuthBlocState {}

class LogoutLoadingState extends AuthBlocState {}

class LogoutFailedState extends AuthBlocState {}

class LogoutSuccessState extends AuthBlocState {}
