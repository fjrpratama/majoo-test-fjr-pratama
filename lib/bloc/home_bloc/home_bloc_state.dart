part of 'home_bloc_cubit.dart';

abstract class HomeBlocState extends Equatable {
  const HomeBlocState();

  @override
  List<Object> get props => [];
}

class HomeBlocInitialState extends HomeBlocState {}

class HomeBlocLoadingState extends HomeBlocState {}

class HomeBlocLoadedState extends HomeBlocState {
  final MovieResponse data;
  HomeBlocLoadedState(this.data);
}

class HomeBlocErrorState extends HomeBlocState {
  final error;
  HomeBlocErrorState(this.error);
}

class GetNewsLoadingState extends HomeBlocState {}

class GetNewsLoadedState extends HomeBlocState {
  final NewsResponseMdl data;
  GetNewsLoadedState(this.data);
}

class GetNewsErrorState extends HomeBlocState {
  final error;
  GetNewsErrorState(this.error);
}
